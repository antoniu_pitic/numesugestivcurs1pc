// f(x,y) = 2*(x-y), daca x<0 si y>0
//			maxim(x,y), daca x<0 si y<=0
//			minim(x+y,2*x), daca x>=0

#include <iostream>
#include "myFunctions.h"

using namespace std;

int main()
{
	int x, y;

	cout << "x="; cin >> x;
	cout << "y="; cin >> y;

	cout << "f(" << x << "," << y << ")=";
	cout << f(x, y);
}